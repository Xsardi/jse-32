package ru.t1.tbobkov.tm.command.system;

import org.jetbrains.annotations.NotNull;
import ru.t1.tbobkov.tm.api.service.IPropertyService;

public final class ApplicationVersionCommand extends AbstractSystemCommand {

    @NotNull
    public static final String ARGUMENT = "-v";

    @NotNull
    public static final String DESCRIPTION = "show version info";

    @NotNull
    public static final String NAME = "version";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @Override
    public void execute() {
        @NotNull final IPropertyService service = getPropertyService();
        System.out.println("[client version]");
        System.out.println(service.getApplicationVersion());
    }

}