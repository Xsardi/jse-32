package ru.t1.tbobkov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.tbobkov.tm.dto.request.TaskListRequest;
import ru.t1.tbobkov.tm.enumerated.Sort;
import ru.t1.tbobkov.tm.model.Task;
import ru.t1.tbobkov.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.List;

public final class TaskListCommand extends AbstractTaskCommand {

    @NotNull
    private static final String NAME = "task-list";

    @NotNull
    private static final String DESCRIPTION = "show task list";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[SHOW TASKS]");
        System.out.println("ENTER SORT:");
        System.out.println(Arrays.toString(Sort.values()));
        @NotNull final String sortType = TerminalUtil.nextLine();
        @Nullable final Sort sort = Sort.toSort(sortType);
        @NotNull final String userId = getUserId();

        @NotNull final TaskListRequest request = new TaskListRequest();
        request.setSort(sort);
        request.setUserId(userId);
        @NotNull final List<Task> tasks =
                getTaskEndpoint().listTask(request).getTasks();
        renderTasks(tasks);
    }

}

