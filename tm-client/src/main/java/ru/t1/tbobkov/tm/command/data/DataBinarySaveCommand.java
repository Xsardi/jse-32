package ru.t1.tbobkov.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.tbobkov.tm.dto.request.DataBinarySaveRequest;

public final class DataBinarySaveCommand extends AbstractDataCommand {

    @NotNull
    public static final String NAME = "data-save-bin";

    @NotNull
    private static final String DESCRIPTION = "save data to binary file";

    @Override
    @SneakyThrows
    public void execute() {
        System.out.println("[BINARY DATA SAVE]");
        @NotNull final DataBinarySaveRequest request = new DataBinarySaveRequest();
        getDomainEndpoint().saveDataBinary(request);
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

}
