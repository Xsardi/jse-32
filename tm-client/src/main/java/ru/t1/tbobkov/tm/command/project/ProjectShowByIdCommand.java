package ru.t1.tbobkov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.tbobkov.tm.dto.request.ProjectGetByIdRequest;
import ru.t1.tbobkov.tm.model.Project;
import ru.t1.tbobkov.tm.util.TerminalUtil;

public final class ProjectShowByIdCommand extends AbstractProjectCommand {

    @NotNull
    private static final String NAME = "project-show-by-id";

    @NotNull
    private static final String DESCRIPTION = "find project by id and show its data";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[SHOW PROJECT BY ID]");
        System.out.println("ENTER ID:");
        @NotNull final String id = TerminalUtil.nextLine();
        @NotNull final String userId = getUserId();

        @NotNull final ProjectGetByIdRequest request = new ProjectGetByIdRequest();
        request.setId(id);
        request.setUserId(userId);
        @Nullable final Project project = getProjectEndpoint().getProjectById(request).getProject();
        if (project == null) return;
        showProject(project);
    }

}
