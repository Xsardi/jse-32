package ru.t1.tbobkov.tm.command;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.tbobkov.tm.api.client.IAuthEndpointClient;
import ru.t1.tbobkov.tm.api.model.ICommand;
import ru.t1.tbobkov.tm.api.service.IServiceLocator;
import ru.t1.tbobkov.tm.enumerated.Role;

public abstract class AbstractCommand implements ICommand {

    protected IServiceLocator serviceLocator;

    public abstract void execute();

    @Nullable
    public abstract String getArgument();

    @NotNull
    public abstract String getDescription();

    @NotNull
    public abstract String getName();

    @Nullable
    public abstract Role[] getRoles();

    @Nullable
    public String getUserId() {
        return getAuthEndpointClient().getUserId();
    }

    @NotNull
    public IServiceLocator getServiceLocator() {
        return serviceLocator;
    }

    public void setServiceLocator(@NotNull final IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    private IAuthEndpointClient getAuthEndpointClient() {
        return serviceLocator.getAuthEndpointClient();
    }

    @NotNull
    @Override
    public String toString() {
        String result = "";
        @NotNull final String name = getName();
        @Nullable final String argument = getArgument();
        @NotNull final String description = getDescription();
        final boolean hasArgument = argument != null && !argument.isEmpty();
        result += name;
        if (hasArgument) result += ", " + argument;
        result += " : " + description;
        return result;
    }

}
