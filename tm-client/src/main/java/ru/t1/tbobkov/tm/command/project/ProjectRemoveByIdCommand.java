package ru.t1.tbobkov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.t1.tbobkov.tm.dto.request.ProjectRemoveByIdRequest;
import ru.t1.tbobkov.tm.util.TerminalUtil;

public final class ProjectRemoveByIdCommand extends AbstractProjectCommand {

    @NotNull
    private static final String NAME = "project-remove-by-id";

    @NotNull
    private static final String DESCRIPTION = "find project by id and remove it from storage";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[REMOVE PROJECT BY ID]");
        System.out.println("ENTER ID:");
        @NotNull final String id = TerminalUtil.nextLine();
        @NotNull final String userId = getUserId();

        @NotNull final ProjectRemoveByIdRequest request = new ProjectRemoveByIdRequest();
        request.setId(id);
        request.setUserId(userId);
        getProjectEndpoint().removeProjectById(request);
    }

}