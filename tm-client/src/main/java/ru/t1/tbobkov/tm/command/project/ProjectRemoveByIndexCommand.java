package ru.t1.tbobkov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.t1.tbobkov.tm.dto.request.ProjectRemoveByIndexRequest;
import ru.t1.tbobkov.tm.util.TerminalUtil;

public final class ProjectRemoveByIndexCommand extends AbstractProjectCommand {

    @NotNull
    private static final String NAME = "project-remove-by-index";

    @NotNull
    private static final String DESCRIPTION = "find project by index and remove it from storage";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[REMOVE PROJECT BY INDEX]");
        System.out.println("ENTER INDEX:");
        @NotNull final Integer index = TerminalUtil.nextNumber() - 1;
        @NotNull final String userId = getUserId();

        @NotNull final ProjectRemoveByIndexRequest request = new ProjectRemoveByIndexRequest();
        request.setIndex(index);
        request.setUserId(userId);
        getProjectEndpoint().removeProjectByIndex(request);
    }

}