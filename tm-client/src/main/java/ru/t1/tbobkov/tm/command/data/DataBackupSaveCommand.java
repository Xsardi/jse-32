package ru.t1.tbobkov.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.tbobkov.tm.dto.request.DataBackupSaveRequest;

public class DataBackupSaveCommand extends AbstractDataCommand {

    @NotNull
    public static final String NAME = "data-save-backup";

    @NotNull
    private static final String DESCRIPTION = "save data as backup";

    @Override
    @SneakyThrows
    public void execute() {
        @NotNull final DataBackupSaveRequest request = new DataBackupSaveRequest();
        getDomainEndpoint().saveDataBackup(request);
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

}
