package ru.t1.tbobkov.tm.api.client;

import ru.t1.tbobkov.tm.api.endpoint.IUserEndpoint;

import java.net.Socket;

public interface IUserEndpointClient extends IUserEndpoint {

    void setSocket(Socket socket);

}
