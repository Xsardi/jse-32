package ru.t1.tbobkov.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.tbobkov.tm.dto.request.DataBase64SaveRequest;

public final class DataBase64SaveCommand extends AbstractDataCommand {

    @NotNull
    public static final String NAME = "data-save-base64";

    @NotNull
    private static final String DESCRIPTION = "save data to base64 file";

    @Override
    @SneakyThrows
    public void execute() {
        System.out.println("[BASE64 DATA SAVE]");
        @NotNull final DataBase64SaveRequest request = new DataBase64SaveRequest();
        getDomainEndpoint().saveDataBase64(request);
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

}
