package ru.t1.tbobkov.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.tbobkov.tm.dto.request.DataBase64LoadRequest;

public class DataBase64LoadCommand extends AbstractDataCommand {

    @NotNull
    public static final String NAME = "data-load-base64";

    @NotNull
    private static final String DESCRIPTION = "load data from base64 file";

    @Override
    @SneakyThrows
    @SuppressWarnings("NullableProblems")
    public void execute() {
        System.out.println("[BASE64 DATA LOAD]");
        @NotNull final DataBase64LoadRequest request = new DataBase64LoadRequest();
        getDomainEndpoint().loadDataBase64(request);
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

}
