package ru.t1.tbobkov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.t1.tbobkov.tm.dto.request.TaskUnbindFromProjectRequest;
import ru.t1.tbobkov.tm.util.TerminalUtil;

public final class TaskUnbindFromProjectCommand extends AbstractTaskCommand {

    @NotNull
    private static final String NAME = "task-remove-from-project";

    @NotNull
    private static final String DESCRIPTION = "remove task from project";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[UNBIND TASK FROM PROJECT]");
        System.out.println("ENTER PROJECT ID:");
        @NotNull final String projectId = TerminalUtil.nextLine();
        System.out.println("ENTER TASK ID:");
        @NotNull final String taskId = TerminalUtil.nextLine();
        @NotNull final String userId = getUserId();

        @NotNull final TaskUnbindFromProjectRequest request = new TaskUnbindFromProjectRequest();
        request.setProjectId(projectId);
        request.setTaskId(taskId);
        request.setUserId(userId);
        getTaskEndpoint().unbindTaskFromProject(request);
    }

}