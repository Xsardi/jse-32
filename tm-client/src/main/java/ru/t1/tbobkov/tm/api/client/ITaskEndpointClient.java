package ru.t1.tbobkov.tm.api.client;

import ru.t1.tbobkov.tm.api.endpoint.ITaskEndpoint;

import java.net.Socket;

public interface ITaskEndpointClient extends ITaskEndpoint {

    void setSocket(Socket socket);

}
