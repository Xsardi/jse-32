package ru.t1.tbobkov.tm.api.client;

import ru.t1.tbobkov.tm.api.endpoint.IAuthEndpoint;

import java.net.Socket;

public interface IAuthEndpointClient extends IAuthEndpoint {

    void setSocket(Socket socket);

    void setUserId(String userId);

    String getUserId();

}
