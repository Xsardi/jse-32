package ru.t1.tbobkov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.tbobkov.tm.enumerated.Role;
import ru.t1.tbobkov.tm.model.User;

public interface IAuthService {

    void checkRoles(@Nullable Role[] roles);

    @NotNull
    User registry(@NotNull String login, @NotNull String password, @NotNull String email);

    void login(@NotNull String login, @NotNull String password);

    void logout();

    boolean isAuth();

    @NotNull
    String getUserId();

    @NotNull
    User getUser();

    @NotNull
    User check(@Nullable String login, @Nullable String password);

}
