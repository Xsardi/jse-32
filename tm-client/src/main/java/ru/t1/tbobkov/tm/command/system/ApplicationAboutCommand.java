package ru.t1.tbobkov.tm.command.system;

import org.jetbrains.annotations.NotNull;
import ru.t1.tbobkov.tm.api.service.IPropertyService;

public final class ApplicationAboutCommand extends AbstractSystemCommand {

    @NotNull
    public static final String ARGUMENT = "-a";

    @NotNull
    public static final String DESCRIPTION = "show info about developer";

    @NotNull
    public static final String NAME = "about";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @Override
    public void execute() {
        @NotNull final IPropertyService service = getPropertyService();
        System.out.println("[application]");
        System.out.println("Name: " + service.getApplicationName());
        System.out.println();

        System.out.println("[about]");
        System.out.println("Name: " + service.getAuthorName());
        System.out.println("E-mail: " + service.getAuthorEmail());
        System.out.println();

        System.out.println("[git]");
        System.out.println("Branch: " + service.getGitBranch());
        System.out.println("Commit Id: " + service.getGitCommitId());
        System.out.println("Commiter: " + service.getGitCommitterName());
        System.out.println("E-Mail: " + service.getGitCommitterEmail());
        System.out.println("Message: " + service.getGitCommitMessage());
        System.out.println("Time: " + service.getGitCommitTime());
    }

}