package ru.t1.tbobkov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.tbobkov.tm.dto.request.ProjectGetByIndexRequest;
import ru.t1.tbobkov.tm.model.Project;
import ru.t1.tbobkov.tm.util.TerminalUtil;

public final class ProjectShowByIndexCommand extends AbstractProjectCommand {

    @NotNull
    private static final String NAME = "project-show-by-index";

    @NotNull
    private static final String DESCRIPTION = "find project by index and show its data";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[SHOW PROJECT BY INDEX]");
        System.out.println("ENTER INDEX:");
        @NotNull final Integer index = TerminalUtil.nextNumber() - 1;
        @NotNull final String userId = getUserId();

        @NotNull final ProjectGetByIndexRequest request = new ProjectGetByIndexRequest();
        request.setIndex(index);
        request.setUserId(userId);
        @Nullable final Project project = getProjectEndpoint().getProjectByIndex(request).getProject();
        if (project == null) return;
        showProject(project);
    }

}