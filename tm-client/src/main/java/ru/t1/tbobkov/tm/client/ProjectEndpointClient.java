package ru.t1.tbobkov.tm.client;

import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.tbobkov.tm.api.client.IProjectEndpointClient;
import ru.t1.tbobkov.tm.dto.request.*;
import ru.t1.tbobkov.tm.dto.response.*;

@NoArgsConstructor
public final class ProjectEndpointClient extends AbstractEndpointClient implements IProjectEndpointClient {

    public ProjectEndpointClient(@NotNull AbstractEndpointClient client) {
        super(client);
    }

    @SneakyThrows
    public static void main(String[] args) {
        @NotNull final AuthEndpointClient authEndpointClient = new AuthEndpointClient();
        authEndpointClient.connect();
        System.out.println(authEndpointClient.login(new UserLoginRequest("test", "test")).getSuccess());

        @NotNull final ProjectEndpointClient projectEndpointClient = new ProjectEndpointClient(authEndpointClient);
        System.out.println(projectEndpointClient.listProject(new ProjectListRequest()).getProjects());

        System.out.println(authEndpointClient.logout(new UserLogoutRequest()).getSuccess());
        authEndpointClient.disconnect();
    }

    @NotNull
    @Override
    public ProjectChangeStatusByIdResponse changeProjectStatusById(@NotNull final ProjectChangeStatusByIdRequest request) {
        return call(request, ProjectChangeStatusByIdResponse.class);
    }

    @NotNull
    @Override
    public ProjectChangeStatusByIndexResponse changeProjectStatusByIndex(@NotNull final ProjectChangeStatusByIndexRequest request) {
        return call(request, ProjectChangeStatusByIndexResponse.class);
    }

    @NotNull
    @Override
    public ProjectClearResponse clearProject(@NotNull final ProjectClearRequest request) {
        return call(request, ProjectClearResponse.class);
    }

    @NotNull
    @Override
    public ProjectCreateResponse createProject(@NotNull final ProjectCreateRequest request) {
        return call(request, ProjectCreateResponse.class);
    }

    @NotNull
    @Override
    public ProjectListResponse listProject(@NotNull final ProjectListRequest request) {
        return call(request, ProjectListResponse.class);
    }

    @NotNull
    @Override
    public ProjectGetByIdResponse getProjectById(@NotNull final ProjectGetByIdRequest request) {
        return call(request, ProjectGetByIdResponse.class);
    }

    @NotNull
    @Override
    public ProjectGetByIndexResponse getProjectByIndex(@NotNull final ProjectGetByIndexRequest request) {
        return call(request, ProjectGetByIndexResponse.class);
    }

    @NotNull
    @Override
    public ProjectUpdateByIdResponse updateProjectById(@NotNull final ProjectUpdateByIdRequest request) {
        return call(request, ProjectUpdateByIdResponse.class);
    }

    @NotNull
    @Override
    public ProjectUpdateByIndexResponse updateProjectByIndex(@NotNull final ProjectUpdateByIndexRequest request) {
        return call(request, ProjectUpdateByIndexResponse.class);
    }

    @NotNull
    @Override
    public ProjectRemoveByIdResponse removeProjectById(@NotNull final ProjectRemoveByIdRequest request) {
        return call(request, ProjectRemoveByIdResponse.class);
    }

    @NotNull
    @Override
    public ProjectRemoveByIndexResponse removeProjectByIndex(@NotNull final ProjectRemoveByIndexRequest request) {
        return call(request, ProjectRemoveByIndexResponse.class);
    }

}
