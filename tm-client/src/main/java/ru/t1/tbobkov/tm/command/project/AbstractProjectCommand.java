package ru.t1.tbobkov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.tbobkov.tm.api.endpoint.IProjectEndpoint;
import ru.t1.tbobkov.tm.command.AbstractCommand;
import ru.t1.tbobkov.tm.enumerated.Role;
import ru.t1.tbobkov.tm.enumerated.Status;
import ru.t1.tbobkov.tm.model.Project;

public abstract class AbstractProjectCommand extends AbstractCommand {

    @NotNull
    public IProjectEndpoint getProjectEndpoint() {
        return getServiceLocator().getProjectEndpointClient();
    }

    @Override
    @Nullable
    public String getArgument() {
        return null;
    }

    @Override
    @Nullable
    public Role[] getRoles() {
        return Role.values();
    }

    protected void showProject(@Nullable final Project project) {
        if (project == null) return;
        System.out.println("ID: " + project.getId());
        System.out.println("NAME: " + project.getName());
        System.out.println("DESCRIPTION: " + project.getDescription());
        System.out.println("STATUS: " + Status.toName(project.getStatus()));
        System.out.println("CREATED: " + project.getCreated());
    }

}
