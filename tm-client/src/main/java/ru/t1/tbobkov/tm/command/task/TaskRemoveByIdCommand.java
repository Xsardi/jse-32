package ru.t1.tbobkov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.t1.tbobkov.tm.dto.request.TaskRemoveByIdRequest;
import ru.t1.tbobkov.tm.util.TerminalUtil;

public final class TaskRemoveByIdCommand extends AbstractTaskCommand {

    @NotNull
    private static final String NAME = "task-remove-by-id";

    @NotNull
    private static final String DESCRIPTION = "find task by id and remove it from storage";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[REMOVE TASK BY ID]");
        System.out.println("ENTER ID:");
        @NotNull final String id = TerminalUtil.nextLine();
        @NotNull final String userId = getUserId();

        @NotNull final TaskRemoveByIdRequest request = new TaskRemoveByIdRequest();
        request.setId(id);
        request.setUserId(userId);
        getTaskEndpoint().removeTaskById(request);
    }

}
