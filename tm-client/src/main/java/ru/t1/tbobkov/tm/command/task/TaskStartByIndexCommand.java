package ru.t1.tbobkov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.t1.tbobkov.tm.dto.request.TaskChangeStatusByIndexRequest;
import ru.t1.tbobkov.tm.enumerated.Status;
import ru.t1.tbobkov.tm.util.TerminalUtil;

public final class TaskStartByIndexCommand extends AbstractTaskCommand {

    @NotNull
    private static final String NAME = "task-start-by-index";

    @NotNull
    private static final String DESCRIPTION = "find task by index and change its status to 'In Progress'";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[START PROJECT BY INDEX]");
        System.out.println("ENTER INDEX:");
        @NotNull final Integer index = TerminalUtil.nextNumber() - 1;
        @NotNull final String userId = getUserId();

        @NotNull final TaskChangeStatusByIndexRequest request = new TaskChangeStatusByIndexRequest();
        request.setStatus(Status.IN_PROGRESS);
        request.setIndex(index);
        request.setUserId(userId);
        getTaskEndpoint().changeTaskStatusByIndex(request);
    }

}