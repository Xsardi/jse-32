package ru.t1.tbobkov.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.tbobkov.tm.dto.request.DataJsonLoadFasterXmlRequest;

public class DataJsonLoadFasterXmlCommand extends AbstractDataCommand {

    @NotNull
    public static final String NAME = "data-load-json-fasterxml";

    @NotNull
    private static final String DESCRIPTION = "load data from xml file";

    @SneakyThrows
    @Override
    public void execute() {
        System.out.println("[JSON DATA LOAD]");
        @NotNull final DataJsonLoadFasterXmlRequest request = new DataJsonLoadFasterXmlRequest();
        getDomainEndpoint().loadDataJsonFasterXml(request);
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

}
