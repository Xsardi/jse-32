package ru.t1.tbobkov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.tbobkov.tm.dto.request.UserProfileRequest;
import ru.t1.tbobkov.tm.enumerated.Role;
import ru.t1.tbobkov.tm.model.User;

public class UserViewProfileCommand extends AbstractUserCommand {

    @NotNull
    private static final String NAME = "user-view";

    @Nullable
    private static final String DESCRIPTION = "view profile of current user";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        @NotNull final String userId = getUserId();

        @NotNull final UserProfileRequest request = new UserProfileRequest();
        request.setUserId(userId);
        @NotNull final User user = getAuthEndpoint().profile(request).getUser();

        System.out.println("[USER VIEW PROFILE]");
        System.out.println("ID: " + user.getId());
        System.out.println("FIRST NAME: " + user.getFirstName());
        System.out.println("MIDDLE NAME: " + user.getMiddleName());
        System.out.println("LAST NAME: " + user.getLastName());
        System.out.println("E-MAIL: " + user.getEmail());
        System.out.println("ROLE: " + user.getRole());
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return Role.values();
    }

}