package ru.t1.tbobkov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import ru.t1.tbobkov.tm.dto.request.UserChangePasswordRequest;
import ru.t1.tbobkov.tm.enumerated.Role;
import ru.t1.tbobkov.tm.util.TerminalUtil;

public class UserChangePasswordCommand extends AbstractUserCommand {

    @NotNull
    private static final String NAME = "user-change-password";

    @NotNull
    private static final String DESCRIPTION = "change password for current user";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        @NotNull final String userId = getUserId();
        System.out.println("[CHANGE USER PASSWORD]");
        System.out.println("ENTER NEW PASSWORD:");
        @NotNull final String password = TerminalUtil.nextLine();

        @NotNull final UserChangePasswordRequest request = new UserChangePasswordRequest();
        request.setPassword(password);
        request.setUserId(userId);
        getUserEndpoint().changeUserPassword(request);
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return Role.values();
    }

}
