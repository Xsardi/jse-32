package ru.t1.tbobkov.tm.command.data;

import org.jetbrains.annotations.NotNull;
import ru.t1.tbobkov.tm.api.endpoint.IDomainEndpoint;
import ru.t1.tbobkov.tm.command.AbstractCommand;
import ru.t1.tbobkov.tm.enumerated.Role;

public abstract class AbstractDataCommand extends AbstractCommand {

    public IDomainEndpoint getDomainEndpoint() {
        return getServiceLocator().getDomainEndpointClient();
    }

    public AbstractDataCommand() {
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

}
