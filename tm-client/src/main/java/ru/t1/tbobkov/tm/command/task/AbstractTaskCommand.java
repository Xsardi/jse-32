package ru.t1.tbobkov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.tbobkov.tm.api.client.IProjectEndpointClient;
import ru.t1.tbobkov.tm.api.endpoint.ITaskEndpoint;
import ru.t1.tbobkov.tm.command.AbstractCommand;
import ru.t1.tbobkov.tm.dto.request.ProjectGetByIdRequest;
import ru.t1.tbobkov.tm.enumerated.Role;
import ru.t1.tbobkov.tm.enumerated.Status;
import ru.t1.tbobkov.tm.model.Project;
import ru.t1.tbobkov.tm.model.Task;

import java.util.List;

public abstract class AbstractTaskCommand extends AbstractCommand {

    @NotNull
    public ITaskEndpoint getTaskEndpoint() {
        return getServiceLocator().getTaskEndpointClient();
    }

    @NotNull
    private IProjectEndpointClient getProjectEndpoint() {
        return getServiceLocator().getProjectEndpointClient();
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return Role.values();
    }

    protected void renderTasks(@NotNull final List<Task> tasks) {
        int index = 1;
        for (@Nullable final Task task : tasks) {
            if (task == null) continue;
            System.out.println(index + ". " + task.getName());
            index++;
        }
    }

    protected void showTask(@Nullable final Task task) {
        if (task == null) return;
        System.out.println("ID: " + task.getId());
        System.out.println("NAME: " + task.getName());
        System.out.println("DESCRIPTION: " + task.getDescription());
        System.out.println("STATUS: " + Status.toName(task.getStatus()));
        System.out.println("PROJECT: " + getProjectName(task));
        System.out.println("CREATED: " + task.getCreated());
    }

    @NotNull
    private String getProjectName(@Nullable final Task task) {
        if (task == null) return "";
        @Nullable final String projectId = task.getProjectId();
        if (projectId == null || projectId.isEmpty()) return "";
        @NotNull ProjectGetByIdRequest request = new ProjectGetByIdRequest();
        request.setId(projectId);
        request.setUserId(getUserId());
        @Nullable final Project project = getProjectEndpoint().getProjectById(request).getProject();
        if (project == null) return "";
        return project.getName();
    }

}
