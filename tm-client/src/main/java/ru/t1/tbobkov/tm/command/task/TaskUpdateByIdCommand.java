package ru.t1.tbobkov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.t1.tbobkov.tm.dto.request.TaskUpdateByIdRequest;
import ru.t1.tbobkov.tm.util.TerminalUtil;

public final class TaskUpdateByIdCommand extends AbstractTaskCommand {

    @NotNull
    private static final String NAME = "task-update-by-id";

    @NotNull
    private static final String DESCRIPTION = "find task by id and update its data";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[UPDATE TASK BY ID]");
        System.out.println("ENTER ID:");
        @NotNull final String id = TerminalUtil.nextLine();
        System.out.println("ENTER NAME:");
        @NotNull final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        @NotNull final String description = TerminalUtil.nextLine();
        @NotNull final String userId = getUserId();

        @NotNull final TaskUpdateByIdRequest request = new TaskUpdateByIdRequest();
        request.setDescription(description);
        request.setName(name);
        request.setId(id);
        request.setUserId(userId);
        getTaskEndpoint().updateTaskById(request);
    }

}
