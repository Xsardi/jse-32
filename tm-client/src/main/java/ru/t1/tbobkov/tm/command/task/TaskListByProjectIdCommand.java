package ru.t1.tbobkov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.t1.tbobkov.tm.dto.request.TaskListByProjectRequest;
import ru.t1.tbobkov.tm.model.Task;
import ru.t1.tbobkov.tm.util.TerminalUtil;

import java.util.List;

public final class TaskListByProjectIdCommand extends AbstractTaskCommand {

    @NotNull
    private static final String NAME = "task-list-by-project-id";

    @NotNull
    private static final String DESCRIPTION = "show task list by project id";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[LIST TASKS BY PROJECT ID]");
        System.out.println("ENTER PROJECT ID:");
        @NotNull final String projectId = TerminalUtil.nextLine();
        @NotNull final String userId = getUserId();

        @NotNull final TaskListByProjectRequest request = new TaskListByProjectRequest();
        request.setProjectId(projectId);
        request.setUserId(userId);
        @NotNull final List<Task> tasks =
                getTaskEndpoint().listTaskByProjectId(request).getTasks();
        renderTasks(tasks);
    }

}