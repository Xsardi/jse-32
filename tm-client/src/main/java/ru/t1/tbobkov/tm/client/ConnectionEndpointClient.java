package ru.t1.tbobkov.tm.client;

import org.jetbrains.annotations.NotNull;
import ru.t1.tbobkov.tm.api.service.IPropertyService;

public final class ConnectionEndpointClient extends AbstractEndpointClient {

    public ConnectionEndpointClient(@NotNull final IPropertyService propertyService) {
        super.setHost(propertyService.getServerHost());
        super.setPort(propertyService.getServerPort());
    }

}
