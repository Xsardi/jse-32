package ru.t1.tbobkov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.tbobkov.tm.api.client.IAuthEndpointClient;
import ru.t1.tbobkov.tm.api.endpoint.IAuthEndpoint;
import ru.t1.tbobkov.tm.api.endpoint.IUserEndpoint;
import ru.t1.tbobkov.tm.command.AbstractCommand;
import ru.t1.tbobkov.tm.exception.user.UserNotFoundException;
import ru.t1.tbobkov.tm.model.User;

public abstract class AbstractUserCommand extends AbstractCommand {

    @NotNull
    public IUserEndpoint getUserEndpoint() {
        return getServiceLocator().getUserEndpointClient();
    }

    @NotNull
    public IAuthEndpoint getAuthEndpoint() {
        return getServiceLocator().getAuthEndpointClient();
    }

    @NotNull
    public IAuthEndpointClient getAuthEndpointClient() {
        return getServiceLocator().getAuthEndpointClient();
    }

    protected void showUser(@Nullable final User user) {
        if (user == null) throw new UserNotFoundException();
        System.out.println("ID: " + user.getId());
        System.out.println("LOGIN: " + user.getLogin());
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

}
