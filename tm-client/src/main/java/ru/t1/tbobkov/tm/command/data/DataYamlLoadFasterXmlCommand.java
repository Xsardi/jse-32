package ru.t1.tbobkov.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.tbobkov.tm.dto.request.DataYamlLoadFasterXmlRequest;

public class DataYamlLoadFasterXmlCommand extends AbstractDataCommand {

    @NotNull
    public static final String NAME = "data-load-yaml";

    @NotNull
    private static final String DESCRIPTION = "load data from yaml file";

    @SneakyThrows
    @Override
    public void execute() {
        System.out.println("[YAML DATA LOAD]");
        @NotNull final DataYamlLoadFasterXmlRequest request = new DataYamlLoadFasterXmlRequest();
        getDomainEndpoint().loadDataYamlFasterXml(request);
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

}
