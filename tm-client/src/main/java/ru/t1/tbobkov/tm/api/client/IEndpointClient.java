package ru.t1.tbobkov.tm.api.client;

import java.io.IOException;
import java.net.Socket;

public interface IEndpointClient {

    Socket connect() throws IOException;

    void disconnect() throws IOException;

}
