package ru.t1.tbobkov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.t1.tbobkov.tm.dto.request.TaskChangeStatusByIdRequest;
import ru.t1.tbobkov.tm.enumerated.Status;
import ru.t1.tbobkov.tm.util.TerminalUtil;

public final class TaskStartByIdCommand extends AbstractTaskCommand {

    @NotNull
    private static final String NAME = "task-start-by-id";

    @NotNull
    private static final String DESCRIPTION = "find task by id and change its status to 'In Progress'";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[START PROJECT BY ID]");
        System.out.println("ENTER ID:");
        @NotNull final String id = TerminalUtil.nextLine();
        @NotNull final String userId = getUserId();

        @NotNull final TaskChangeStatusByIdRequest request = new TaskChangeStatusByIdRequest();
        request.setStatus(Status.IN_PROGRESS);
        request.setId(id);
        request.setUserId(userId);
        getTaskEndpoint().changeTaskStatusById(request);
    }

}