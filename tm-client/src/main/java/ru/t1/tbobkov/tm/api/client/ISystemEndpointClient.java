package ru.t1.tbobkov.tm.api.client;

import ru.t1.tbobkov.tm.api.endpoint.ISystemEndpoint;

import java.net.Socket;

public interface ISystemEndpointClient extends ISystemEndpoint {

    void setSocket(Socket socket);

}
