package ru.t1.tbobkov.tm.api.client;

import ru.t1.tbobkov.tm.api.endpoint.IDomainEndpoint;

import java.net.Socket;

public interface IDomainEndpointClient extends IDomainEndpoint {

    void setSocket(Socket socket);

}
