package ru.t1.tbobkov.tm.api.client;

import ru.t1.tbobkov.tm.api.endpoint.IProjectEndpoint;

import java.net.Socket;

public interface IProjectEndpointClient extends IProjectEndpoint {

    void setSocket(Socket socket);

}
