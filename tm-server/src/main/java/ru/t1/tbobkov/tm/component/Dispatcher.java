package ru.t1.tbobkov.tm.component;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.tbobkov.tm.api.endpoint.Operation;
import ru.t1.tbobkov.tm.dto.request.AbstractRequest;
import ru.t1.tbobkov.tm.dto.response.AbstractResponse;
import ru.t1.tbobkov.tm.exception.entity.OperationEmptyException;

import java.util.LinkedHashMap;
import java.util.Map;

public class Dispatcher {

    @NotNull
    private final Map<Class<? extends AbstractRequest>, Operation<?, ?>> map = new LinkedHashMap<>();

    public <RQ extends AbstractRequest, RS extends AbstractResponse> void registry(
            @NotNull final Class<RQ> reqClass, @NotNull final Operation<RQ, RS> operation
    ) {
        map.put(reqClass, operation);
    }

    @NotNull
    @SuppressWarnings({"unchecked", "rawtypes"})
    public Object call(@NotNull final AbstractRequest request) {
        @Nullable final Operation operation = map.get(request.getClass());
        if (operation == null) throw new OperationEmptyException();
        return operation.execute(request);
    }

}
