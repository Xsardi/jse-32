package ru.t1.tbobkov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.tbobkov.tm.api.service.IAuthService;
import ru.t1.tbobkov.tm.api.service.IPropertyService;
import ru.t1.tbobkov.tm.api.service.IUserService;
import ru.t1.tbobkov.tm.exception.field.LoginEmptyException;
import ru.t1.tbobkov.tm.exception.field.PasswordEmptyException;
import ru.t1.tbobkov.tm.exception.user.IncorrectLoginDataException;
import ru.t1.tbobkov.tm.model.User;
import ru.t1.tbobkov.tm.util.HashUtil;

public class AuthService implements IAuthService {

    @NotNull
    private final IUserService userService;

    @NotNull
    private final IPropertyService propertyService;

    public AuthService(@NotNull final IPropertyService propertyService, @NotNull final IUserService userService) {
        this.userService = userService;
        this.propertyService = propertyService;
    }

    @NotNull
    @Override
    public User registry(@NotNull final String login, @NotNull final String password, @NotNull final String email) {
        return userService.create(login, password, email);
    }

    @NotNull
    @Override
    public User check(@Nullable String login, @Nullable String password) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        @Nullable final User user = userService.findByLogin(login);
        if (user == null) throw new IncorrectLoginDataException();
        final boolean locked = user.getLocked();
        if (locked) throw new IncorrectLoginDataException();
        @Nullable final String hash = HashUtil.salt(propertyService, password);
        if (hash != null && !hash.equals(user.getPasswordHash())) throw new IncorrectLoginDataException();
        return user;
    }

}
