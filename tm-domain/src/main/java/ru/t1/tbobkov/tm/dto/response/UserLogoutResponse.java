package ru.t1.tbobkov.tm.dto.response;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;

@Getter
@Setter
public final class UserLogoutResponse extends AbstractResultResponse {

    public UserLogoutResponse() {
        setSuccess(true);
    }

    public UserLogoutResponse(@NotNull Throwable throwable) {
        super(throwable);
    }

}
