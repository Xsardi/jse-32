package ru.t1.tbobkov.tm.dto.response;

import lombok.Getter;
import lombok.Setter;
import ru.t1.tbobkov.tm.model.Project;

@Getter
@Setter
public final class ProjectGetByIndexResponse extends AbstractResponse {

    private Project project;

    public ProjectGetByIndexResponse(Project project) {
        this.project = project;
    }

}
