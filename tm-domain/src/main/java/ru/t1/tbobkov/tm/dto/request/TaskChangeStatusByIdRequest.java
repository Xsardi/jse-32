package ru.t1.tbobkov.tm.dto.request;

import lombok.Getter;
import lombok.Setter;
import ru.t1.tbobkov.tm.enumerated.Status;

@Getter
@Setter
public final class TaskChangeStatusByIdRequest extends AbstractUserRequest {

    private String id;

    private Status status;

}
