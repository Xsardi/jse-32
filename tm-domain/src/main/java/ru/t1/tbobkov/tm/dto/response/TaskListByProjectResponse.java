package ru.t1.tbobkov.tm.dto.response;

import lombok.Getter;
import lombok.Setter;
import ru.t1.tbobkov.tm.model.Task;

import java.util.List;

@Getter
@Setter
public final class TaskListByProjectResponse extends AbstractResponse {

    private List<Task> tasks;

    public TaskListByProjectResponse(List<Task> tasks) {
        this.tasks = tasks;
    }

}
