package ru.t1.tbobkov.tm.dto.response;

import lombok.Getter;
import lombok.Setter;
import ru.t1.tbobkov.tm.model.Task;

import java.util.List;

@Getter
@Setter
public final class TaskListResponse extends AbstractResponse {

    private List<Task> tasks;

    public TaskListResponse(List<Task> tasks) {
        this.tasks = tasks;
    }

}
