package ru.t1.tbobkov.tm.dto.response;

import lombok.Getter;
import lombok.Setter;
import ru.t1.tbobkov.tm.model.Task;

@Getter
@Setter
public final class TaskGetByIndexResponse extends AbstractResponse {

    private Task task;

    public TaskGetByIndexResponse(Task task) {
        this.task = task;
    }

}
