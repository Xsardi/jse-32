package ru.t1.tbobkov.tm.dto.request;

import lombok.Getter;
import lombok.Setter;
import ru.t1.tbobkov.tm.enumerated.Status;

@Getter
@Setter
public final class TaskChangeStatusByIndexRequest extends AbstractUserRequest {

    private Integer index;

    private String userId;

    private Status status;

}
