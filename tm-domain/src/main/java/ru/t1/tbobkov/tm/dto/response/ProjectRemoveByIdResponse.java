package ru.t1.tbobkov.tm.dto.response;

import lombok.Getter;
import lombok.Setter;
import ru.t1.tbobkov.tm.model.Project;

@Getter
@Setter
public final class ProjectRemoveByIdResponse extends AbstractResponse {

    private Project project;

    public ProjectRemoveByIdResponse(Project project) {
        this.project = project;
    }

}
