package ru.t1.tbobkov.tm.dto.request;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public final class UserUpdateProfileRequest extends AbstractUserRequest {

    private String firstName;

    private String middleName;

    private String lastName;

}
