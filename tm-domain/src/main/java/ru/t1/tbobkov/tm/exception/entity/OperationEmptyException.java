package ru.t1.tbobkov.tm.exception.entity;

public final class OperationEmptyException extends AbstractEntityException {

    public OperationEmptyException() {
        super("Error! Empty operation called...");
    }

}
