package ru.t1.tbobkov.tm.exception.user;

public class AccessDeniedException extends AbstractUserException {

    public AccessDeniedException() {
        super("Error! Access denied...");
    }

}
