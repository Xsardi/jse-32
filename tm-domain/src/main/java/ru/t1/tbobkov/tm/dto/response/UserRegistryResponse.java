package ru.t1.tbobkov.tm.dto.response;

import lombok.Getter;
import lombok.Setter;
import ru.t1.tbobkov.tm.model.User;

@Getter
@Setter
public final class UserRegistryResponse extends AbstractResponse {

    private User user;

    public UserRegistryResponse(User user) {
        this.user = user;
    }

}
