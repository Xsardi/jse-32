package ru.t1.tbobkov.tm.dto.response;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;

@Getter
@Setter
public final class UserLoginResponse extends AbstractResultResponse {

    private String userId;

    public UserLoginResponse() {
        setSuccess(true);
    }

    public UserLoginResponse(@NotNull Throwable throwable) {
        super(throwable);
    }

}
