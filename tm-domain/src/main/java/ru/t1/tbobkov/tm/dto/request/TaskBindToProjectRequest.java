package ru.t1.tbobkov.tm.dto.request;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public final class TaskBindToProjectRequest extends AbstractUserRequest {

    private String projectId;

    private String taskId;

}
