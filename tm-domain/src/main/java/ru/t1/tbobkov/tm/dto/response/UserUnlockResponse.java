package ru.t1.tbobkov.tm.dto.response;

import lombok.Getter;
import lombok.Setter;
import ru.t1.tbobkov.tm.model.User;

@Getter
@Setter
public final class UserUnlockResponse extends AbstractResponse {

    private User user;

    public UserUnlockResponse(User user) {
        this.user = user;
    }

}
