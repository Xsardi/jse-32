package ru.t1.tbobkov.tm.dto.response;

import lombok.Getter;
import lombok.Setter;
import ru.t1.tbobkov.tm.model.User;

@Getter
@Setter
public final class UserRemoveResponse extends AbstractResponse {

    private User user;

    public UserRemoveResponse(User user) {
        this.user = user;
    }

}
