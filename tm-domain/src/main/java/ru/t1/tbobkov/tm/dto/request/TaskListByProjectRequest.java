package ru.t1.tbobkov.tm.dto.request;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public final class TaskListByProjectRequest extends AbstractUserRequest {

    private String projectId;

}
